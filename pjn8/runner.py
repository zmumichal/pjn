# -*- coding: utf-8 -*-
import codecs
from copy import copy
import locale
import re
from gensim import corpora, models, similarities
from collections import defaultdict
from pprint import pprint
import gensim
import sys
from forms import get_form_to_representative
import logging
from sklearn.metrics.pairwise import cosine_similarity

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


def get_documents():
    # raw = codecs.open('lab8/pap_short.txt', encoding='utf8', errors='ignore').read()
    raw = codecs.open('lab8/pap.txt', encoding='utf8', errors='ignore').read()
    preprocessed = re.sub(r'#\d+', 'SPLIT_HERE', raw, flags=re.UNICODE)
    raw_result = re.split(r'SPLIT_HERE', preprocessed, flags=re.UNICODE)[1:]

    preprocessed = re.sub(r'[^\w\s]|\d+', '', preprocessed, flags=re.UNICODE)
    result = re.split(r'SPLIT_HERE', preprocessed, flags=re.UNICODE)[1:]
    return result, raw_result


def get_form_to_base_mapping():
    form_lines = codecs.open('lab8/odm.utf8.txt', encoding='utf8', errors='ignore').readlines()
    return get_form_to_representative(form_lines)


def get_frequencies(texts):
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1
    return frequency


def print_alg_matches(score_vectors, alg_token):
    similarities = [cosine_similarity(score_vectors[note_number], score) for score in score_vectors]
    for i in range(1, matches_count + 1):
        result = max(similarities)
        index_of_best = similarities.index(result)
        similarities[index_of_best] = 0
        print "\n\n---------------------------------"
        print alg_token, ">>> Dopasowanie nr", i, "wynik:", result, raw_documents[index_of_best]

        score_vector = copy(score_vectors[index_of_best])
        for j in range(topics_count):
            result = max(score_vector)
            index_of_best_topic = score_vector.index(result)
            score_vector[index_of_best_topic] = 0
            print alg_token, "\t#topic", lsi.print_topic(index_of_best_topic)


if __name__ == "__main__":
    form_to_base = get_form_to_base_mapping()
    documents, raw_documents = get_documents()

    documents = documents[:200]#todo remove

    texts = [[word.lower() for word in document.lower().split()] for document in documents]
    texts = [[form_to_base[word].lower() if word in form_to_base else word for word in text] for text in texts]

    stop_list = set(u'w on się a z u czy do o to już za na i być że który ten my mieć przez jak dla po'.split())
    texts = [[word for word in text if word not in stop_list] for text in texts]

    frequency = get_frequencies(texts)
    texts = [[token for token in text if frequency[token] > 1] for text in texts]

    id2word = corpora.Dictionary(texts)
    corpus = [id2word.doc2bow(text) for text in texts]

    lsi = gensim.models.lsimodel.LsiModel(corpus=corpus, id2word=id2word, num_topics=200)
    lsi.save('pap.lsi')

    lda = gensim.models.ldamodel.LdaModel(corpus=corpus, id2word=id2word, num_topics=200, update_every=0, passes=20)
    lda.save('pap.lda')


    # document_lda = [[val[1] for val in lda[bow]] for bow in corpus]
    lsi_score_vectors = [[val[1] for val in lsi.__getitem__(bow, scaled=True)] for bow in corpus]
    lda_score_vectors = [[val[1] for val in lda.__getitem__(bow, eps=-1)] for bow in corpus]

    while True:
        line = raw_input("numer notatki: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
        note_number = int(line)
        line = raw_input("ilość pasujących: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
        matches_count = int(line)
        # matches_count = 3
        line = raw_input("ilość tematów: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
        topics_count = int(line)
        # topics_count = 3

        print "\n\n------ WYBRANA: ------", raw_documents[note_number]

        print_alg_matches(lsi_score_vectors, "LSI")
        print_alg_matches(lda_score_vectors, "LDA")


