# -*- coding: utf-8 -*-

class CGTokens:
    SENTENCE_BREAK = 'SENTENCE_BREAK'
    IGNORE = 'IGNORE'

    all = {SENTENCE_BREAK, IGNORE}

    def __init__(self):
        pass


class CGState:
    LOOKING_FOR_PREPOSITION = 1
    MATCHING_CASES = 2
    MATCHING_CASES_AFTER_SUBSTANTIVE = 3

    def __init__(self, status=LOOKING_FOR_PREPOSITION):
        self.status = status
        self.preposition = None
        self.accumulator = []

    def push(self, data):
        self.accumulator[-1]['data'].append(data)

    def get_accumulated(self):
        return filter(lambda x: len(x['data']) > 0, self.accumulator)

    def quit_preposition_context(self):
        self.preposition = None
        self.status = CGState.LOOKING_FOR_PREPOSITION

    def enter_preposition_context(self, preposition):
        self.preposition = preposition
        self.accumulator.append({'prep': preposition, 'data': []})
        self.status = CGState.MATCHING_CASES


class CaseGovernmentFSM():
    CONTEXT_PRESERVING_KINDS = {
        u'conj',  #spójnik
        u'adv',  #przysłówek
    }
    CONTEXT_BREAKER_KINDS = {
        u'verb',  #czasownik
        u'prep',  #przyimek
    }
    KINDS_ALWAYS_MATCHED = {
        u'pact',  #imiesłów przymiotnikowy czynny
        u'ppron12'  #zaimek nietrzecioosobowy
        u'ppron3'  #zaimek trzecioosobowy
        u'num',  #liczebnik
        u'ppas',  #imiesłów przymiotnikowy bierny
        u'adj',  #przymiotnik
    }
    KINDS_LIKE_SUBSTANTIVE = {
        u'ger',  #rzeczownik odsłowny
        u'subst',  #rzeczownik
        u'c',  #forma deprecjatywna
    }

    def __init__(self, form_info, interesting_prepositions=None):
        self.interesting_prepositions = interesting_prepositions
        self.form_info = form_info
        self.state = CGState()

    def get_info(self, form):
        return self.form_info[form] if self.form_info.has_key(form) else None

    def is_interesting_preposition(self, form):
        should_ignore = self.interesting_prepositions is not None and form not in self.interesting_prepositions

        info = self.get_info(form)
        return u'prep' in info['kinds'] and not should_ignore

    def feed_token(self, token):
        if token is None:
            return

        if token in CGTokens.all:
            if token == CGTokens.IGNORE:
                return
            elif token == CGTokens.SENTENCE_BREAK:
                self.state.quit_preposition_context()

        else:
            info = self.get_info(token)
            if info is None:
                self.state.quit_preposition_context()
            else:
                if self.is_interesting_preposition(token):
                    self.state.enter_preposition_context(token)
                else:
                    for kind in info['kinds']:
                        if self.state.status == CGState.MATCHING_CASES:
                            if kind in CaseGovernmentFSM.CONTEXT_BREAKER_KINDS:
                                self.state.quit_preposition_context()
                            elif kind in CaseGovernmentFSM.KINDS_ALWAYS_MATCHED:
                                self.state.push(token)
                            elif kind in CaseGovernmentFSM.KINDS_LIKE_SUBSTANTIVE:
                                self.state.push(token)
                                self.state.status = CGState.MATCHING_CASES_AFTER_SUBSTANTIVE
                            elif kind in CaseGovernmentFSM.CONTEXT_PRESERVING_KINDS:
                                pass
                            else:
                                self.state.quit_preposition_context()
                        elif self.state.status == CGState.MATCHING_CASES_AFTER_SUBSTANTIVE:
                            if kind in CaseGovernmentFSM.CONTEXT_BREAKER_KINDS:
                                self.state.quit_preposition_context()
                            elif kind in CaseGovernmentFSM.KINDS_ALWAYS_MATCHED:
                                self.state.push(token)
                            elif kind in CaseGovernmentFSM.KINDS_LIKE_SUBSTANTIVE:
                                self.state.quit_preposition_context()
                            elif kind in CaseGovernmentFSM.CONTEXT_PRESERVING_KINDS:
                                pass
                            else:
                                self.state.quit_preposition_context()