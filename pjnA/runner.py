# -*- coding: utf-8 -*-
import codecs
import re
import operator
from morfologik_extractor import get_form_to_raw_description, get_form_info
from fsm import CaseGovernmentFSM
from fsm import CGTokens


def get_restricted_corpus(filename='pap.txt', prepositions=[]):
    raw = codecs.open(filename, encoding='utf8', errors='ignore').read()

    raw = re.sub(r'\s+', ' ', raw, flags=re.UNICODE)
    pattern_core = "|".join(prepositions + [w.capitalize() for w in prepositions])
    sentence_parts = re.findall(r'((' + pattern_core + ')\s.*?)(\.\s+[A-Z]|\n\n+)', raw, flags=re.UNICODE | re.DOTALL)
    extracted = [r[0] for r in sentence_parts]
    wrapped_break = ' ' + CGTokens.SENTENCE_BREAK + '\n'
    raw = wrapped_break.join(extracted)

    preprocessed = re.sub(r'[^\w\s]|\d+', '', raw, flags=re.UNICODE)
    preprocessed = preprocessed.lower()
    return preprocessed, raw


def to_words_with_tokens_only(text):
    wrapped_break = ' ' + CGTokens.SENTENCE_BREAK + ' '
    wrapped_ignore = ' ' + CGTokens.IGNORE + ' '

    text = re.sub(r' i ', wrapped_ignore, text, flags=re.UNICODE)
    text = re.sub(r'http://[\w.]+', wrapped_ignore, text, flags=re.UNICODE)

    text = re.sub(r'\.\s+(?=[A-Z])', wrapped_break, text, flags=re.UNICODE)
    text = re.sub(r'\.\s+\n\n+', wrapped_break, text, flags=re.UNICODE)
    text = re.sub(r'[^\w\s]|\d+', ' ', text, flags=re.UNICODE)
    text = re.sub(r'\s+', ' ', text, flags=re.UNICODE)

    text = text.lower()
    text = re.sub(wrapped_break.lower(), wrapped_break, text, flags=re.UNICODE)
    text = re.sub(wrapped_ignore.lower(), wrapped_ignore, text, flags=re.UNICODE)

    return text.split(r" ")


def record_prepostitions_data(text, form_info, prepositions):
    tokens = to_words_with_tokens_only(text)
    fsm = CaseGovernmentFSM(form_info, prepositions)

    for token in tokens:
        fsm.feed_token(token)
    return fsm.state.get_accumulated()


def get_preposition_to_case_percentages(recorded, form_info):
    result = {}

    for entry in recorded:
        matched_count = len(entry['data'])
        if matched_count == 0:
            continue

        case_frequencies = {u'acc': 0, u'nom': 0, u'loc': 0, u'voc': 0, u'dat': 0, u'inst': 0, u'gen': 0}
        for matched in entry['data']:
            info = form_info[matched]
            kinds_count = len(info['kinds'])

            for kind in info['kinds']:
                for case in info['cases'][kind] if info['cases'].has_key(kind) else []:
                    case_frequencies[case] += 1. / kinds_count / matched_count

        preposition = entry['prep']
        if result.has_key(preposition):
            result[preposition].append(case_frequencies)
        else:
            result[preposition] = [case_frequencies, ]

    return result


prepositions = [
    'dla',
    'pod',
    'z',
    'u',
    'bez',
    'do',
    # 'od',
    # 'przez',
    # 'oprócz',
    # 'przy',
    # 'przed',
    # 'według',
    # 'przeciw',
    # 'jak',
    # 'na',
    # 'o',
    # 'powyżej',
    # 'wobec',
    # 'względem',
    # 'jako',
    # 'dzięki',
    # 'wokół',
    # 'wbrew',
    # 'po',
]

lower_words, raw = get_restricted_corpus(filename='pap.txt', prepositions=prepositions)
form_to_raw_description = get_form_to_raw_description(lower_words)
form_info = get_form_info(form_to_raw_description)

recorded = record_prepostitions_data(raw, form_info, prepositions)
preposition_to_case_percentage = get_preposition_to_case_percentages(recorded, form_info)

for preposition, occurrences_lists in preposition_to_case_percentage.items():
    case_frequencies = {u'acc': 0, u'nom': 0, u'loc': 0, u'voc': 0, u'dat': 0, u'inst': 0, u'gen': 0}
    total = len(occurrences_lists)

    for case in case_frequencies.keys():
        for list in occurrences_lists:
            divider = max(list.values())
            if divider > 0:
                case_frequencies[case] += pow(list[case] / divider, 3)  #penalizing low internal correlation

    print "\n\n---------------------------"
    print "\t\t---", preposition, "---"

    print "\nBy lib:"
    for line in form_to_raw_description[preposition]:
        print "\t", line

    print "\nBy fsm:"
    divider = max(case_frequencies.values())
    for case, count in sorted(case_frequencies.items(), key=operator.itemgetter(1), reverse=True):
        if count > 0:
            print "\t", case, "with relative score", count / divider

    print "\n...with number of complex samples: ", len(occurrences_lists)