# -*- coding: utf-8 -*-
from pprint import pprint
import re
import subprocess


def get_morfologik_output(unicode_input):
    cmd = ['java', '-jar', 'morfologik-tools-1.9.0-standalone.jar', 'plstem']
    morfologik = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    raw = morfologik.communicate(b'%s\n' % unicode_input.encode(u'utf-8'))[0]
    trimmed = re.split(r"\nProcessed", raw, flags=re.UNICODE)
    print "Processed", trimmed[1]
    return unicode(trimmed[0], encoding='utf-8')


def get_form_to_raw_description(text):
    output = get_morfologik_output(text)
    lines = re.split(r'\n', output, flags=re.UNICODE)
    lines_split = [re.split(r'\s', line, flags=re.UNICODE) for line in lines]

    form_to_raw_description = {}
    for i in lines_split:
        if len(i) > 2 and ':' in i[2]:
            if not form_to_raw_description.has_key(i[0]):
                form_to_raw_description[i[0]] = set()
            form_to_raw_description[i[0]].add(i[2])

    return form_to_raw_description


def get_words_having_grammar_cases(form_to_listed_descriptions):
    grammar_cases = {u'acc', u'nom', u'loc', u'voc', u'dat', u'inst', u'gen'}
    result = set()

    for form, descriptions in form_to_listed_descriptions.items():
        for case in grammar_cases:
            for description in descriptions:
                if case in description:
                    result.add(description.split(':')[0])

    return result


def descriptions_to_mappings(form_to_list_of_listed_attributes):
    ret = {}
    for form, descriptions in form_to_list_of_listed_attributes.iteritems():
        tmp = {}
        for description in descriptions:
            kind = description[0]
            cases = set(description[1])
            if not tmp.has_key(kind):
                tmp[kind] = cases
            else:
                tmp[kind] = tmp[kind] | cases

        ret[form] = {}
        ret[form]['kinds'] = tmp.keys()
        for kind, cases in tmp.iteritems():
            if len(cases) > 0:
                if ret[form].has_key('cases'):
                    ret[form]['cases'][kind] = cases
                else:
                    ret[form]['cases'] = {kind: cases}
    return ret


def get_form_info(form_to_raw_description):
    form_to_listed_descriptions = {
        form: [desc for listed in [description.split('+') for description in descriptions] for desc in listed] for
        form, descriptions in form_to_raw_description.items()
    }

    # word_kinds_with_cases = get_words_having_grammar_cases(form_to_listed_descriptions)
    word_kinds_with_cases = {
        u'pact',  #imiesłów przymiotnikowy czynny
        u'ger',  #rzeczownik odsłowny
        u'ppron',  #zaimek dzierżawczy
        u'subst',  #rzeczownik
        u'num',  #liczebnik
        u'ppas',  #imiesłów przymiotnikowy bierny
        u'depr',  #forma deprecjatywna
        u'adj',  #przymiotnik
    }

    form_to_list_of_listed_attributes = {
        form: [
            [split[0], split[2].split('.') if len(split) > 2 and split[0] in word_kinds_with_cases else []]
            for split in [description.split(':') for description in descriptions]
        ] for form, descriptions in form_to_listed_descriptions.items()
    }

    return descriptions_to_mappings(form_to_list_of_listed_attributes)
