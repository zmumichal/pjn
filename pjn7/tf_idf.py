# -*- coding: utf-8 -*-
import codecs
from collections import OrderedDict
from pprint import pprint
import re
from math import log
import operator
from forms import get_counts_sorted, get_form_to_representative, get_words_from_text


def get_tf_idf_matrix(form_to_repr, documents):
    document_count = len(documents)
    tfs_per_document = map(lambda x: get_counts_sorted(form_to_repr, x), documents)

    all_base_forms = map(lambda word: form_to_repr[word] if form_to_repr.has_key(word) else word,
                         set(get_words_from_text(raw)))
    dfs_existence = map(lambda doc_scope: set(map(lambda tfs: tfs[0], doc_scope)), tfs_per_document)
    dfs = dict(map(lambda form: (form, sum(map(lambda x: 1 if form in x else 0, dfs_existence))), all_base_forms))

    tfs_idfs = map(lambda tfs:
                   map(lambda term_to_tf:
                       (term_to_tf[0], term_to_tf[1] * log(document_count / dfs[term_to_tf[0]], 10)), tfs),
                   tfs_per_document)

    return tfs_idfs


def get_keywords(tfs_idf, count=6):
    ordered_tf_idf = map(lambda el: OrderedDict(sorted(el, key=lambda t: t[1])), tfs_idf)
    best_idf = map(lambda dict: map(lambda i: dict.popitem()[0] if len(dict) > 0 else u'', range(count)),
                   ordered_tf_idf)
    return best_idf


if __name__ == "__main__":
    form_lines = codecs.open('lab7/odm.utf8.txt', encoding='utf8', errors='ignore').readlines()
    form_to_repr = get_form_to_representative(form_lines)

    raw = codecs.open('lab7/pap_short.txt', encoding='utf8', errors='ignore').read()
    # raw = codecs.open('lab7/pap.txt', encoding='utf8', errors='ignore').read()
    documents = re.split(r'#\d+', raw, flags=re.UNICODE)[1:]

    tfs_idf = get_tf_idf_matrix(form_to_repr, documents)
    keywords = get_keywords(tfs_idf, 6)

    for i in range(len(documents)):
        print documents[i]
        print '\n#' + (', #'.join(keywords[i]))
        print '\n----------\n'
        raw_input("more... ")
