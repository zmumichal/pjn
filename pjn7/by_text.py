# -*- coding: utf-8 -*-
import codecs
import locale
from pprint import pprint
import re
import sys
from math import sqrt
from forms import get_form_to_representative, get_counts_sorted
from forms import get_words_from_text
from tf_idf import get_tf_idf_matrix


def compute_cosine_metric(first, second):
    first_length = sqrt(reduce(lambda x, y: x + y, map(lambda x: x * x, first.values()), 0))
    second_length = sqrt(reduce(lambda x, y: x + y, map(lambda x: x * x, second.values()), 0))

    result = 0
    for key in set(first.keys()) & set(second.keys()):
        result += first[key] * second[key]

    if first_length * second_length == 0:
        return 0

    return result / first_length / second_length


if __name__ == "__main__":
    form_lines = codecs.open('lab7/odm.utf8.txt', encoding='utf8', errors='ignore').readlines()
    form_to_repr = get_form_to_representative(form_lines)

    raw = codecs.open('lab7/pap_short.txt', encoding='utf8', errors='ignore').read()
    # raw = codecs.open('lab7/pap.txt', encoding='utf8', errors='ignore').read()
    documents = re.split(r'#\d+', raw, flags=re.UNICODE)[1:]
    tfs_per_document = map(lambda x: dict(get_counts_sorted(form_to_repr, x)), documents)

    while True:
        line = raw_input("text: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
        tfs_of_input = dict(get_counts_sorted(form_to_repr, line))

        distances = map(lambda tfs: compute_cosine_metric(tfs_of_input, tfs), tfs_per_document)

        index_of_best = distances.index(max(distances))
        print documents[index_of_best]
