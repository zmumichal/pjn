# -*- coding: utf-8 -*-
import codecs
import locale
from operator import itemgetter
from pprint import pprint
import re
import sys
from math import log
import operator
from forms import get_form_to_representative, get_counts_sorted
from forms import get_words_from_text
from tf_idf import get_tf_idf_matrix


if __name__ == "__main__":
    form_lines = codecs.open('lab7/odm.utf8.txt', encoding='utf8', errors='ignore').readlines()
    form_to_repr = get_form_to_representative(form_lines)

    raw = codecs.open('lab7/pap_short.txt', encoding='utf8', errors='ignore').read()
    # raw = codecs.open('lab7/pap.txt', encoding='utf8', errors='ignore').read()
    documents = re.split(r'#\d+', raw, flags=re.UNICODE)[1:]

    tfs_per_document = map(lambda x: get_counts_sorted(form_to_repr, x), documents)
    all_base_forms = map(lambda word: form_to_repr[word] if form_to_repr.has_key(word) else word,
                         set(get_words_from_text(raw)))
    dfs_existence = map(lambda doc_scope: set(map(lambda tfs: tfs[0], doc_scope)), tfs_per_document)

    dfs = dict(map(lambda form: (form, sum(map(lambda x: 1 if form in x else 0, dfs_existence))), all_base_forms))
    tfs_idfs = map(lambda tfs:
                   map(lambda term_to_tf:
                       (term_to_tf[0], term_to_tf[1] * log(len(documents) / dfs[term_to_tf[0]], 10)), tfs),
                   tfs_per_document)

    tfs_idfs_maxes = map(lambda tfs: max(tfs, key=operator.itemgetter(1))[1], tfs_idfs)

    for i in range(len(tfs_idfs)):
        tfs_idfs[i] = dict(map(lambda x: (x[0], x[1] / tfs_idfs_maxes[i]), tfs_idfs[i]))

    while True:
        line = raw_input("words: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
        words = get_words_from_text(line)
        as_representatives = map(lambda word: form_to_repr[word] if form_to_repr.has_key(word) else word, words)

        coverage = []
        for doc_nr in range(len(tfs_idfs)):
            coverage_tmp = 0
            for repr_nr in range(len(as_representatives)):
                repr = as_representatives[repr_nr]
                coverage_tmp += tfs_idfs[doc_nr][repr] if repr in tfs_idfs[doc_nr] else 0
            coverage += [coverage_tmp, ]

        index_of_best = coverage.index(max(coverage))
        print documents[index_of_best]


