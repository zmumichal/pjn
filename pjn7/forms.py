import codecs
from collections import Counter
import re


def get_form_to_representative(form_lines):
    forms = map(lambda line: map(lambda x: x.strip(), line.split(',')), form_lines)
    form_to_representative = {}
    for form_line in forms:
        for form in form_line:
            form_to_representative[form.strip().lower()] = form_line[0]
    return form_to_representative


def get_words_from_text(target):
    words = [match.group(1) for match in re.finditer(r'(?=(\b\w+\b))', target, flags=re.UNICODE)]
    return map(lambda word: word.strip().lower(), words)


def get_counts_sorted(form_to_repr, target):
    words = get_words_from_text(target)

    as_representatives = map(lambda word: form_to_repr[word] if form_to_repr.has_key(word) else word, words)

    return Counter(as_representatives).most_common()


