# -*- coding: utf-8 -*-
import cProfile
import codecs
import re
from markov import Markov


def extract_ngrams(text, len=4):
    text = '. ' + text
    text = re.sub('#\d+', ' ', text, flags=re.UNICODE)
    text = re.sub('[^\w\s.,]|_|\d', ' ', text, flags=re.UNICODE)
    text = re.sub('\. ([A-Z])', '. \g<1>', text, flags=re.UNICODE)
    text = re.sub(',', ' , ', text, flags=re.UNICODE)
    text = re.sub('\s+', ' ', text, flags=re.UNICODE)

    regexp = r'(\b\S+|[,.])\s'
    for i in range(len - 2):
        regexp += r'(\S+|[,.])\s'
    regexp += r'(\S+\b|[,.]?)'
    return [match.group(1) for match in re.finditer(r'(?=(' + regexp + r'))', text, flags=re.UNICODE)]


def main():
    text = codecs.open('lab5/pap.txt', encoding='utf8', errors='ignore').read()
    markov = Markov()
    markov.add_ngrams(extract_ngrams(text, 3))

    while raw_input('next? ') != "x":
        print markov.generate()


if __name__ == "__main__":
    main()
    # cProfile.run('main()')
