# -*- coding: utf-8 -*-
import cProfile
import codecs
import random
import re
from markov import Markov


def extract_ngrams(text, len=4):
    text = '. ' + text
    text = re.sub('(\w)', '\g<1> ', text, flags=re.UNICODE)
    regexp = r'(\b\S+|[,.])\s'
    for i in range(len - 2):
        regexp += r'(\S+|[,.])\s'
    regexp += r'(\S+\b|[,.])'
    return [match.group(1) for match in re.finditer(r'(?=(' + regexp + r'))', text, flags=re.UNICODE)]


def main():
    lines = codecs.open('lab5/formy.utf8.txt', encoding='utf8', errors='ignore').readlines()
    markov = Markov()

    i = 0
    for line in lines:
        if i % 10000 == 0:
            print i, ' out of ', len(lines)
        i += 1

        ngrams = extract_ngrams(line, 4)
        markov.add_ngrams(ngrams)

    while raw_input('next? ') != "x":
        print markov.generate(random.randint(5, 10))


if __name__ == "__main__":
    main()
    #cProfile.run('main()')
