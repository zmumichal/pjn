import random
import re


class Markov():
    # first_to_successors = {
    #     'ma': {'ma kota': 2, 'ma psa': 1},
    #     'psa': {'psa przewodnika': 1},
    #     'kota': {'kota nieroba': 1}
    # }
    # first_to_successor_count = {'ma': 3, 'psa': 1, 'kota': 1}
    def __init__(self):
        random.seed()

    first_to_successors = {}
    first_to_successor_count = {}

    def _push_to_successors(self, first, ngram):
        if first not in self.first_to_successors:
            self.first_to_successors[first] = {}
            self.first_to_successor_count[first] = 0

        successors = self.first_to_successors[first]
        if ngram not in successors:
            successors[ngram] = 0

        successors[ngram] += 1
        self.first_to_successor_count[first] += 1


    def add_ngrams(self, ngrams):
        i = 0
        nlen = len(ngrams)
        for ngram in ngrams:
            i += 1
            if i % 100000 == 0:
                print i, " of ", nlen
            tokens = ngram.split(u" ")

            first = u" ".join(tokens[:-1])
            self._push_to_successors(first, ngram)

    def get_next_for(self, last):
        if last not in self.first_to_successor_count:
            return False

        choice = random.randint(1, self.first_to_successor_count[last])
        for ngram, count in self.first_to_successors[last].iteritems():
            choice -= count
            if choice <= 0:
                return ngram

    def generate(self, steps=50):
        random.seed()
        init_index = random.randint(0, len(self.first_to_successors) - 1)
        last = list(self.first_to_successors.iteritems())[init_index][0]

        result = []
        for i in range(steps):
            ngram = self.get_next_for(last)
            if ngram == False:
                break

            last = u" ".join(ngram.split(u' ')[1:])
            result.append(ngram.split(u' ')[0])

        print result
        text = u' '.join(result)
        text += u' ' + last
        text = re.sub(' \.', '.', text, flags=re.UNICODE)
        return re.sub(' ,', ',', text, flags=re.UNICODE)
