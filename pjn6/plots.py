import codecs
import numpy as np
from math import log
import math
import matplotlib.pyplot as plt
from forms import get_counts_sorted
from scipy.optimize import curve_fit


def zipf_proto(x, k):
    return k / x


def mandelbrot_proto(x, P, B, d):
    sum = np.add(x, d)
    sumlog = np.log10(sum)
    logp = np.log10(P)
    logf = logp - B * sumlog
    return 10 ** logf


def find_zipf(counts_only):
    domain = range(1, len(counts_only) + 1)
    param, covs = curve_fit(zipf_proto, domain, counts_only)
    print "k = ", param[0]
    return [zipf_proto(x, param[0]) for x in domain]


def find_madnelbrot(counts_only):
    domain = range(1, len(counts_only) + 1)
    param, covs = curve_fit(mandelbrot_proto, domain, counts_only)
    print "(P B d) = ", param
    return [mandelbrot_proto(x, param[0], param[1], param[2]) for x in domain]


if __name__ == "__main__":
    form_lines = codecs.open('lab6/odm.utf8.txt', encoding='utf8', errors='ignore').readlines()
    target = codecs.open('lab6/potop.txt', encoding='utf8', errors='ignore').read()
    counts_sorted = get_counts_sorted(form_lines, target)

    counts_only = map(lambda x: x[1], counts_sorted)
    zipf = find_zipf(counts_only)
    mandelbrot = find_madnelbrot(counts_only)

    plt.plot(zipf, 'r')
    plt.plot(mandelbrot, 'g')
    plt.plot(counts_only, 'b')
    plt.yscale('log')
    plt.show()