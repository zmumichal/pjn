import locale
from pjn1.solver import Solver


files = {
    'polish': ['polski.txt', 'polski2.txt', 'polski3.txt'],
    'italian': ['q.txt', '54.txt'],
    'german': ['2momm10.txt', '4momm10.txt', '5momm10.txt', '8momm10.txt', ],
    'finnish': ['finnish.txt', 'finnish1.txt'],
    'spanish': ['spanish.txt', 'spanish1.txt'],
    'english': [
        'Harry Potter 1 Sorcerer\'s_Stone.txt',
        'Harry Potter 2 Chamber_of_Secrets.txt',
        'Harry Potter 3 Prisoner of Azkaban.txt',
        'Harry Potter 4 and the Goblet of Fire.txt',
    ],
}

solver = Solver(files, '../pjn_lab1/', 3)

input = raw_input("Feed line: ")
while input != 'q':
    match = solver.calculateMatch(input)
    print match, solver.certainity(match)
    input = raw_input("Feed line: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))