import chardet
from math import sqrt
import re
import codecs
import operator


class TextPreprocessor():
    def __init__(self):
        pass

    def filter(self, text):
        preprocessed = text.lower()
        preprocessed = re.sub('[^\w\s]', ' ', preprocessed, flags=re.UNICODE)
        preprocessed = re.sub('\s+', ' ', preprocessed, flags=re.UNICODE)
        preprocessed = re.sub('\d', '', preprocessed, flags=re.UNICODE)
        return preprocessed

    def splitToNgrams(self, text, length=3):
        matches = re.finditer(r'(?=(.{' + str(length) + '}))', text, flags=re.UNICODE)
        return [match.group(1) for match in matches]

    def countNgrams(self, ngramsList):
        ngramToCount = dict()
        for i in ngramsList:
            ngramToCount[i] = ngramToCount.get(i, 0) + 1
        return ngramToCount

    def process(self, text, length=3):
        return self.countNgrams(self.splitToNgrams(self.filter(text), length))


def getEncodedTextFromFile(filename, sample=1000):
    notEncoded = open(filename)
    detected = chardet.detect(notEncoded.read()[0:sample])
    # print filename, detected
    source = codecs.open(filename, encoding=detected['encoding'], errors='ignore')
    return source.read()


def computeMetric(first, second):
    firstLength = sqrt(reduce(lambda x, y: x + y, map(lambda x: x * x, first.values()), 0))
    secondLength = sqrt(reduce(lambda x, y: x + y, map(lambda x: x * x, second.values()), 0))

    result = 0
    for key in set(first.keys()) & set(second.keys()):
        result += first[key] * second[key]
    return result / firstLength / secondLength


def computeNgramStats(filenames, directory,ngramsLength):
    preprocessor = TextPreprocessor()
    texts = map(lambda filename: getEncodedTextFromFile(directory + filename), filenames)
    filteredTexts = map(lambda text: preprocessor.filter(text), texts)
    ngramsLists = map(lambda text: preprocessor.splitToNgrams(text, ngramsLength), filteredTexts)
    ngramsLists = reduce(lambda list, cumulated: list + cumulated, ngramsLists, [])
    return preprocessor.countNgrams(ngramsLists)


class Solver():
    def __init__(self, files, dir, ngramLength=3):
        self.ngramLength = ngramLength
        self.data = dict(map(lambda (lang, files): (lang, computeNgramStats(files, dir, ngramLength)), files.iteritems()))

    def calculateMatch(self, input):
        userStats = TextPreprocessor().process(input, self.ngramLength)
        matches = dict(map(lambda (language, stat): (language, computeMetric(stat, userStats)), self.data.iteritems()))
        return sorted(matches.items(), key=operator.itemgetter(1), reverse=True)

    @staticmethod
    def certainity(matches):
        if matches[0][1] == 0:
            return 0
        return matches[0][1] / (matches[0][1] + matches[1][1])