# -*- coding: utf-8 -*-
import time
from pjn1.solver import Solver


files = {
    'polish': ['polski.txt', 'polski2.txt', 'polski3.txt'],
    'italian': ['q.txt', '54.txt'],
    'german': ['2momm10.txt', '4momm10.txt', '5momm10.txt', '8momm10.txt', ],
    'finnish': ['finnish.txt', 'finnish1.txt'],
    'spanish': ['spanish.txt', 'spanish1.txt'],
    'english': [
        'Harry Potter 1 Sorcerer\'s_Stone.txt',
        'Harry Potter 2 Chamber_of_Secrets.txt',
        'Harry Potter 3 Prisoner of Azkaban.txt',
        'Harry Potter 4 and the Goblet of Fire.txt',
    ],
}

sentences = {
    'polish': [
        'Według byłego zawodnika w zespole jest kilku bardzo poważnych kandydatów do tego miana.',
        'Niektórzy mówią, że chorował. Ja nie zauważyłem, choć często go widywałem',
        ' Podmiotem lirycznym jest tu sam poeta, który pozostawia Mecenasowi dyspozycje co do postępowania po jego śmierci. ',
        'Przemieniony poeta odbywa lot ponad ziemią, który symbolizuje jego wyjątkową pozycję wśród ludzi (został on obdarzony niezwykłym talentem)',
        'Ratunkiem na kłopoty finansowe mógłby być ślub z Anielą, pochodzącą z bogatej rodziny.',
        'Można nawet uznać, że jest to rewolucja, zwłaszcza w sensie wykreowania głównego bohatera.',
    ],
    'italian': [
        'L\'opera ebbe subito uno straordinario successo, e contribuì in maniera determinante al processo di consolidamento del dialetto toscano come lingua italiana',
        ' È una delle letture obbligate del sistema scolastico italiano.',
        'Il poema spesso finisce con il compimento della missione da parte dell\'eroe.',
        'Il termine può acquisire ulteriori precisazioni o significati negli ambiti filosofico, religioso o nelle arti.',
        'Una tecnica di recitazione questa comune anche alla commedia dell\'arte.',
    ],
    'german': [
        'Bedingung des Mitleids ist die Nähe, d. h. faktisches Mitleid kann sich immer nur auf anschaulich gegebenes Leid beziehen. ',
        'Mitleid als episodisches (vorübergehendes) Gefühl ist auch Gegenstand der Literatur und Literaturtheorie.',
        'In der stoischen Philosophie wurde das Mitleid dagegen explizit abgelehnt. ',
        'So kann etwa die Urteilskraft eines schwer kranken Menschen ebenso stark geschwächt sein wie sein Allgemeinzustand.',
        'Im Gegenzug kann der Helfende der Situation nicht oder nicht ausreichend gewachsen sein.',
        ' Einige von ihnen verbieten ihren Anhängern aber gesellschaftlich vereinbarte und tradierte Hilfsangebote.',
    ],
    'finnish': [
        'Suomi kuuluu maapallon ilmastovyöhykkeistä pääosin subarktiseen mannerilmastoon.',
        'Jälkimmäisessä virassa Niinistö tunnettiin tiukasta talouskuristaan ja työstään 1990-luvun laman jälkeisten säästöjen ja leikkausten parissa.',
        'Kokoomuksen kansanedustaja Ilkka Kanerva on kertonut epäilleensä tulokasta vahvasti ja pitäneensä häntä yhden kauden kansanedustajana. ',
        'Varatuomarin arvonimen voi myöntää hakemuksesta se hovioikeus, jonka tuomiopiirissä tuomioistuinharjoittelu on suoritettu. ',
        'Yleensä harjoittelu suoritetaan melko pian valmistumisen jälkeen.',
    ],
    'spanish': [
        ' Del primero se hizo caso omiso y del segundo finalmente fructificó el actual calendario mundial.',
        'Conforme a la tradición católica, el papado tiene su origen en san Pedro, apóstol de Jesús que fue constituido como primer papa y a quien se le otorgó la dirección de la Iglesia y el primado apostólico.',
        'Sin embargo, seguía utilizándose indistintamente para otros obispos. ',
        'Por tales motivos Pedro es considerado dentro de la Iglesia Católica como el primer papa. ',
        'Más importante es el caso de Pedro, a quien los católicos considera que suceden los 265 papas que después de él han regido la Iglesia Católica Romana.',
        'Cabe destacar, que dichos padres de la Iglesia, parecen aseverar además la primacía de la iglesia de Roma.',
    ],
    'english': [
        'Popes, who originally had no temporal powers, in some periods of history accrued wide powers similar to those of temporal rulers.',
        'Thus the revival of the temporal power of the pope was deemed impossible. ',
        'When referring to manuscripts, this may be a revision by another author.',
        'The process is essentially the same - in Europe special presentation impressions of prints were often printed on silk until the seventeenth century. ',
        'Children sometimes grow up in such families with the understanding that such an arrangement is normal. ',
    ],
}

#warmup... (dynamic cpu power management...)
Solver(files, '../pjn_lab1/', 3)

solvers = {}
for n in [1, 2, 3, 4, 6, 8]:
    start = time.clock()
    solvers[n] = Solver(files, '../pjn_lab1/', n)
    print "Prepared solver for ngram-len", n, "in", time.clock() - start

for n in solvers.keys():
    print "\n-----------> ngram-len: ", n
    languageSum = 0
    languageCount = 0
    languageStart = time.clock()
    for language in sentences.keys():
        sum = 0
        count = 0
        start = time.clock()
        for sentence in sentences[language]:
            match = solvers[n].calculateMatch(sentence)
            if match[0][0] == language:
                sum += Solver.certainity(match)
            count += 1
        certainty = sum / count
        elapsed = (time.clock() - start)
        print "\t", language, "\tngram-len:", n, "\telapsed:", elapsed, "\tcertainity:", certainty, "\tcert./time:", certainty / elapsed
        languageCount += count
        languageSum += sum

    certainty = languageSum / languageCount
    elapsed = (time.clock() - languageStart)
    print "Overal", "\tngram-len:", n, "\telapsed:", elapsed, "\tcertainity:", certainty, "\tcert./time:", certainty / elapsed