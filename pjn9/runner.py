# -*- coding: utf-8 -*-
import codecs
from collections import Counter
import locale
from pprint import pprint
import re
from math import sqrt
import math
import operator
import sys
from forms import get_form_to_representative


def get_documents():
    raw = codecs.open('lab9/pap.txt', encoding='utf8', errors='ignore').read()
    preprocessed = re.sub(r'#\d+', 'SPLIT_HERE', raw, flags=re.UNICODE)
    raw_result = re.split(r'SPLIT_HERE', preprocessed, flags=re.UNICODE)[1:]

    preprocessed = re.sub(r'[^\w\s]|\d+', '', preprocessed, flags=re.UNICODE)
    result = re.split(r'SPLIT_HERE', preprocessed, flags=re.UNICODE)[1:]
    return result, raw_result


def get_form_to_base_mapping():
    form_lines = codecs.open('lab9/odm.utf8.txt', encoding='utf8', errors='ignore').readlines()
    return get_form_to_representative(form_lines)


def get_stop_list(texts):
    manual = set(u'w on się a z u czy do o to już za na i być że który ten my mieć przez jak dla po ze'.split())
    occurrences = Counter([word for text in texts for word in text])
    most_popular = set(map(lambda x: x[0], occurrences.most_common(40)))
    return manual | most_popular


def get_edges(text, k):
    return set.union(*map(lambda x: set(zip(text, text[x:])), range(0, k + 2)))


def get_edges_up_to_degree(text, degree):
    k = degree
    partials = map(lambda x: zip(text, text[x:]), range(0, k + 2))
    result = [partials[0] + partials[1], ]

    for i in range(1, k + 1):
        result += [result[-1] + partials[i + 1], ]
    return result


def cosine_distance(lhs, rhs):
    lhsLength = math.sqrt(sum(map(lambda x: x[1] * x[1], lhs.most_common())))
    rhsLength = math.sqrt(sum(map(lambda x: x[1] * x[1], rhs.most_common())))

    result = 0
    for key in set(lhs.keys()) & set(rhs.keys()):
        result += lhs[key] * rhs[key]
    return result / lhsLength / rhsLength if lhsLength * rhsLength != 0 else 0


if __name__ == "__main__":
    form_to_base = get_form_to_base_mapping()
    documents, raw_documents = get_documents()

    # documents = documents[:1000]  #todo remove

    texts = [[word.lower() for word in document.lower().split()] for document in documents]
    texts = [[form_to_base[word].lower() if word in form_to_base else word for word in text] for text in texts]

    stop_list = get_stop_list(texts)
    texts = [[word for word in text if word not in stop_list] for text in texts]

    k = 4
    graphs = [get_edges_up_to_degree(text, k) for text in texts]
    graphs_with_counts = [[Counter(graph) for graph in graphs_of_orders] for graphs_of_orders in graphs]

    raw_notes_numbers = raw_input("provide 10 note numbers ").decode(
        sys.stdin.encoding or locale.getpreferredencoding(True))
    note_numbers = map(int, raw_notes_numbers.split(u" "))

    notes = [graphs_with_counts[number - 1] for number in note_numbers]

    for i in range(len(note_numbers)):
        note_number = note_numbers[i]
        note_counters = notes[i]

        for k in range(len(note_counters)):
            distances = [cosine_distance(note_counters[k], graph[k]) for graph in graphs_with_counts]
            index_with_distance = zip(range(len(distances)), distances)
            ordered = sorted(index_with_distance, key=operator.itemgetter(1), reverse=True)

            for item in ordered[:10]:
                print " ----------------------------------- "
                print "k = ", k, ", note = ", note_number, ", matched = ", item[0] + 1, ", score = ", item[1]
                print documents[item[0] + 1]

                wants_skip = raw_input("skip? (y,N) ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
                if wants_skip == u"y":
                    break

