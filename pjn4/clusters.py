class Dbscan():
    def region_query(self, distances_vector, eps):
        result = []
        for i in range(len(distances_vector)):
            if distances_vector[i] <= eps:
                result.append(i)
        return result

    def expand_cluster(self, core, distances_matrix, eps):
        result = set()
        for i in core:
            result = result.union(set(self.region_query(distances_matrix[i], eps)))
        return result

    def fit(self, distances_matrix, eps, core_min_size):
        elements_count = len(distances_matrix)
        visited = [False] * elements_count
        labels = [-1] * elements_count
        cluster_no = 0

        for i in range(elements_count):
            if not visited[i]:
                core = self.region_query(distances_matrix[i], eps)
                if len(core) >= core_min_size:
                    cluster = self.expand_cluster(core, distances_matrix, eps)
                    for v in cluster:
                        visited[v] = True
                        labels[v] = cluster_no
                    cluster_no += 1
        return labels