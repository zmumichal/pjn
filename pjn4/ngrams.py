import cProfile
from math import sqrt
from collections import Counter
import re
from itertools import product
from scipy.spatial import distance
from scipy.spatial.distance import cosine


class Preprocessor():
    STOPLIST = ['address', 'tel', 'fax', 'str', 'no', 'city', 'ul', 'ltd', 'road', 'p\s*o\s*box',
                'sp', 'z\s*o\s*o', 'limited', 'nip', 'co', 'gmb', 'town', 'village', 'nr', 'room',
                'company', 'zip', 'building', 'th', 'floor', 'street', 'district', 'area', 'zone',
                'quay of the', 'lc inn', 'kpp', 'by order of', 'order of', 'same as above', 'to order',
                'same as consignee', 'same as', 'doroha na', 'contact'
    ]

    def remove_numbers(self, text):
        text = re.sub('\d', '', text)
        return text

    def remove_single_letters(self, text):
        text = re.sub('\\b\w\\b', '', text)
        return text

    def remove_stoplisted(self, text):
        # print reduce(lambda builded, pattern: builded + '|(\\b' + pattern + '\\b)', self.STOPLIST,'')[1:]
        for pattern in self.STOPLIST:
            text = re.sub('\\b' + pattern + '\\b', '', text)
        return text

    def remove_multiple_spaces(self, text):
        text = re.sub(' +', ' ', text)
        return text

    def remove_non_interesting_chars(self, text):
        text = re.sub('[^\w\s]|_', ' ', text)
        return text

    def wrap_numbers_with_spaces(self, text):
        text = re.sub('(\d+)', ' \g<1> ', text)
        return text

    def __init__(self, source):
        text = source.lower()
        text = self.remove_non_interesting_chars(text)
        text = self.remove_numbers(text)
        # text = self.wrap_numbers_with_spaces(text)
        text = self.remove_single_letters(text)
        text = self.remove_stoplisted(text)
        text = self.remove_multiple_spaces(text)
        self.text = text
        self.source = source

    def get_ngrams(self, length=3):
        prepared = self.text.splitlines()
        raw = self.source.splitlines()

        result = []
        for i in range(len(raw)):
            splitted = [match.group(1) for match in re.finditer(r'(?=(.{' + str(length) + '}))', prepared[i])]
            result.append([raw[i], Counter(splitted)])

        self.get_ngrams_base(length)
        return result

    def get_ngrams_base(self, length):
        all_chars_in_text = set(self.text)
        all_chars_in_text.remove('\n')
        return map(lambda list: "".join(list), list(product(all_chars_in_text, repeat=length)))

    def get_raw_ngrams(self, length=3):
        base = self.get_ngrams_base(length)

        prepared = self.text.splitlines()

        list_of_ngram_to_count = map(
            lambda line: Counter([match.group(1) for match in re.finditer(r'(?=(.{' + str(length) + '}))', line)]),
            prepared)
        return map(lambda ngrams: map(lambda token: ngrams[token], base), list_of_ngram_to_count)


def compare_ngrams(lhs, rhs):
    l_ngrams = lhs[1]
    r_ngrams = rhs[1]

    l_length = sqrt(reduce(lambda x, y: x + y, map(lambda x: x * x, l_ngrams.values()), 0))
    r_length = sqrt(reduce(lambda x, y: x + y, map(lambda x: x * x, r_ngrams.values()), 0))

    result = 0.0
    for key in set(l_ngrams.keys()) & set(r_ngrams.keys()):
        result += l_ngrams[key] * r_ngrams[key]
    return 1 - (result / l_length / r_length)



    # cProfile.run("Preprocessor(open('./lab4/lines.txt').read()).get_raw_ngrams(2)")
    # all = Preprocessor(open('./lab4/lines.txt').read()).get_raw_ngrams(2)
    # cProfile.run("distance.squareform(distance.pdist(all,'cosine'))")