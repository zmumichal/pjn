import cProfile
from collections import Counter
from scipy.spatial import distance
import datetime
import operator
import numpy as np
from ngrams import Preprocessor
from sklearn.cluster import DBSCAN
from clusters import Dbscan


def output_comparision_file(measurements):
    file = open(str(datetime.datetime.now()) + '_results.txt', 'w+')

    f1_of_set = reduce(lambda v, entry: v + entry['f1'], measurements, 0.) / len(measurements)
    recall_of_set = reduce(lambda v, entry: v + entry['recall'], measurements, 0.) / len(measurements)
    precision_of_set = reduce(lambda v, entry: v + entry['precision'], measurements, 0.) / len(measurements)
    print f1_of_set, map(lambda entry: entry['f1'], measurements)
    file.write('mean: f1:\t')
    file.write(str(f1_of_set))
    file.write('\n\tprecision:\t')
    file.write(str(precision_of_set))
    file.write('\n\trecall:\t')
    file.write(str(recall_of_set))
    file.write('\n\n')

    for entry in measurements:
        file.write('f1:\t')
        file.write(str(entry['f1']))

        file.write('\t\tprecision:\t')
        file.write(str(entry['precision']))

        file.write('\t\trecall:\t')
        file.write(str(entry['recall']))

        file.write('\nreference:')
        for line in entry['reference']:
            file.write('\n\t' + line)

        file.write('\ncorresponding cluster:')
        for line in entry['cluster']:
            file.write('\n\t' + line)

        file.write('\n\n\n')

    file.close()


def calculate_measurements(reference, clusterized, line_to_cluster):
    result = []
    for reference_cluster in reference:
        what_clusters = Counter(map(lambda line: line_to_cluster[line], reference_cluster))
        max_count = max(what_clusters.iteritems(), key=operator.itemgetter(1))[1]
        clusters_with_max_count = [clusterized[k] for (k, v) in what_clusters.iteritems() if v == max_count]

        corresponding_cluster = sorted(clusters_with_max_count, key=len)[0]

        true_positives = float(len(reference_cluster & corresponding_cluster))
        false_positives = float(len(corresponding_cluster - reference_cluster))
        false_negatives = float(len(reference_cluster - corresponding_cluster))

        precision = true_positives / (true_positives + false_positives)
        recall = true_positives / (true_positives + false_negatives)
        f1 = 0. if precision + recall == 0 else 2. * precision * recall / (precision + recall)

        result.append({'f1': f1,
                       'precision': precision,
                       'recall': recall,
                       'reference': reference_cluster,
                       'cluster': corresponding_cluster})
    return result


def read_reference_file(reference_filename):
    reference = [set()]
    for line in (open(reference_filename)).readlines():
        line = line.strip()
        if line == "":
            pass
        elif line.startswith("#"):
            reference.append(set())
        else:
            reference[-1].add(line)

    return filter(lambda i: len(i) > 0, reference)


def output_clusters_file(clusterized):
    file = open(str(datetime.datetime.now()) + '_clusters.txt', 'w+')
    for (key, values) in clusterized.iteritems():
        for line in values:
            file.write(line)
            file.write('\n')
        file.write("\n\n##########\n")
    file.close()


def get_precomputed_distances_from_file(filename):
    data = Preprocessor(open(filename).read()).get_raw_ngrams(2)
    precomputed = distance.squareform(distance.pdist(data, 'cosine'))
    are_nan = np.isnan(precomputed)
    precomputed[are_nan] = 1
    return precomputed


def get_clusterized(filename):
    precomputed = get_precomputed_distances_from_file(filename)

    # db = DBSCAN(eps=0.15, min_samples=1, metric='precomputed', algorithm='auto', leaf_size=30).fit(precomputed)
    # labels = db.labels_

    labels = Dbscan().fit(precomputed, 0.15, 1)

    grouped = {}
    lines = open(filename).readlines()

    for line_number in range(len(labels)):
        group_number = labels[line_number]
        line = lines[line_number].strip()
        if not grouped.has_key(group_number):
            grouped[group_number] = set()
        grouped[group_number].add(line)

    mapping = {}
    for line_number in range(len(labels)):
        line = lines[line_number].strip()
        mapping[line] = labels[line_number]

    return grouped, mapping


def main():
    filename = './lab4/lines.txt'
    reference_filename = 'lab4/clusters.txt'
    reference = read_reference_file(reference_filename)

    clusterized, line_to_cluster = get_clusterized(filename)
    output_clusters_file(clusterized)

    measurements = calculate_measurements(reference, clusterized, line_to_cluster)
    output_comparision_file(measurements)

    f1_of_set = reduce(lambda v, entry: v + entry['f1'], measurements, 0.) / len(measurements)
    print f1_of_set, map(lambda entry: entry['f1'], measurements)

if __name__ == "__main__":
    cProfile.run('main()')
