# -*- coding: utf-8 -*-
import itertools
import locale
import sys
from prolevenstein import levenstein
import cProfile
from operator import itemgetter
from statistics import Statistics
from Levenshtein.StringMatcher import StringMatcher


def get_distance(given, correction):
    # distance = levenstein(given, correction)
    distance = StringMatcher(seq1=given, seq2=correction).distance()
    return distance


def get_how_probable(given, correction, statistics, error_stats):
    distance = get_distance(given, correction)
    of_distance = error_stats.getProbabilityOfDistance(distance)
    laplaced = statistics.laplaced(correction)
    return of_distance * pow(laplaced, 0.1)
