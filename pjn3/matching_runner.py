# -*- coding: utf-8 -*-
import itertools
import locale
import sys
from error_stats import ErrorStats
from pjn import get_how_probable
from prolevenstein import levenstein
import cProfile
from operator import itemgetter
from statistics import Statistics
from Levenshtein.StringMatcher import StringMatcher

statistics = Statistics('lab3/pocz.utf8.dat', 'lab3/konc.utf8.dat')
statistics.read('lab3/dramat.iso.utf8')
statistics.read('lab3/popul.iso.utf8')
statistics.read('lab3/proza.iso.utf8')
statistics.read('lab3/publ.iso.utf8')
statistics.read('lab3/wp.iso.utf8')

error_stats = ErrorStats('lab3/bledy.txt')

RETAINED_COUNT = 600


def main(to_correct, statistics):
    qualified = [["", -1] for x in range(0, RETAINED_COUNT)]
    worst_score = -1
    worst_index = 0

    if to_correct in statistics.form_to_prefix.keys():
        return 'valid word'

    for prefix in statistics.valid_forms_counts.keys():
        score = get_how_probable(to_correct, prefix, statistics, error_stats)
        if score > worst_score:
            qualified[worst_index] = [prefix, score]
            worst_score = score
            for i in range(0, len(qualified)):
                if qualified[i][1] < worst_score:
                    worst_score = qualified[i][1]
                    worst_index = i

    qualified = map(lambda (prefix, score): statistics.valid_forms_counts[prefix]['forms'], qualified)
    qualified = list(itertools.chain(*qualified))

    result = map(lambda form: [form, get_how_probable(to_correct, form, statistics, error_stats)], qualified)
    result.sort(key=itemgetter(1), reverse=True)
    return result


to_correct = raw_input("Feed line: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
# cProfile.run('print main(to_correct, statistics)')
print main(to_correct, statistics)