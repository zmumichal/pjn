import codecs
import re


MULTIPLIER = float(1)


class Statistics:
    def __init__(self, prefixes_filename, suffixes_filename):
        self.valid_count = 0
        self.total_read_count = 0
        self.valid_forms_counts = {}
        self.form_to_prefix = {}

        grouped_suffixes = map(
            lambda line: line.split(';')[2].split(':')[:-1],
            codecs.open(suffixes_filename, encoding='utf8', errors='ignore').readlines()
        )

        for line in codecs.open(prefixes_filename, encoding='utf8', errors='ignore').readlines():
            entry = line.strip().split(':')
            prefix = entry[0]

            suffixes = grouped_suffixes[int(entry[1])] if len(entry) == 2 else ["", ]
            forms = {prefix + suffix: 0 for suffix in suffixes}
            for form in forms.keys():
                self.form_to_prefix[form] = prefix

            if not self.valid_forms_counts.has_key(prefix):
                self.valid_forms_counts[prefix] = {'count': 0, 'forms': {}}
            self.valid_forms_counts[prefix]['forms'].update(forms)
            self.valid_count += 1

    def read(self, filename):
        source = codecs.open(filename, encoding='utf8', errors='ignore')

        preprocessed = source.read().lower()
        preprocessed = re.sub('[^\w\s]', ' ', preprocessed, flags=re.UNICODE)
        preprocessed = re.sub('\s+', ' ', preprocessed, flags=re.UNICODE)
        preprocessed = re.sub('\d', '', preprocessed, flags=re.UNICODE)

        words = preprocessed.strip().split(' ')
        self.total_read_count += len(words)

        for word in words:
            if self.form_to_prefix.has_key(word):
                entry = self.valid_forms_counts[self.form_to_prefix[word]]
                entry['count'] += 1
                entry['forms'][word] += 1

    def laplacedForPrefix(self, word):
        if self.form_to_prefix.has_key(word):
            entry = self.valid_forms_counts[self.form_to_prefix[word]]
            return MULTIPLIER * (entry['count'] + 1) / (self.total_read_count + self.valid_count)
        else:
            return MULTIPLIER * 1 / (self.total_read_count + self.valid_count)

    def laplaced(self, word):
        if self.form_to_prefix.has_key(word):
            entry = self.valid_forms_counts[self.form_to_prefix[word]]
            return MULTIPLIER * (entry['forms'][word] + 1) / (self.total_read_count + self.valid_count)
        else:
            return MULTIPLIER * 1 / (self.total_read_count + self.valid_count)