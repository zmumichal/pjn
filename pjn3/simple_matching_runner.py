# -*- coding: utf-8 -*-
import itertools
import locale
import sys
from error_stats import ErrorStats
from pjn import get_how_probable
from prolevenstein import levenstein
import cProfile
from operator import itemgetter
from statistics import Statistics
from Levenshtein.StringMatcher import StringMatcher

statistics = Statistics('lab3/pocz.utf8.dat', 'lab3/konc.utf8.dat')
statistics.read('lab3/dramat.iso.utf8')
statistics.read('lab3/popul.iso.utf8')
statistics.read('lab3/proza.iso.utf8')
statistics.read('lab3/publ.iso.utf8')
statistics.read('lab3/wp.iso.utf8')

error_stats = ErrorStats('lab3/bledy.txt')

RETAINED_COUNT = 600


def main(to_correct, statistics):
    if to_correct in statistics.form_to_prefix.keys():
        return 'valid word'

    result = map(lambda form: [form, get_how_probable(to_correct, form, statistics, error_stats)],
                 statistics.form_to_prefix.keys())
    result.sort(key=itemgetter(1), reverse=True)
    return result


to_correct = raw_input("Feed line: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
# cProfile.run('print main(to_correct, statistics)[:20]')
for entry in main(to_correct, statistics)[:20]:
    # print entry[0], entry[1]
    print entry[0], entry[1], statistics.valid_forms_counts[statistics.form_to_prefix[entry[0]]]['forms'][entry[0]]

# print main(to_correct, statistics)[:20]