import locale
import sys
from error_stats import ErrorStats
from pjn import get_how_probable
from prolevenstein import levenstein
from statistics import Statistics

statistics = Statistics('lab3/pocz.utf8.dat', 'lab3/konc.utf8.dat')
statistics.read('lab3/dramat.iso.utf8')
statistics.read('lab3/popul.iso.utf8')
statistics.read('lab3/proza.iso.utf8')
statistics.read('lab3/publ.iso.utf8')
statistics.read('lab3/wp.iso.utf8')

error_stats = ErrorStats('lab3/bledy.txt')

while True:
    given = raw_input("given: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
    correction = raw_input("correction: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
    print get_how_probable(given, correction, statistics, error_stats)
