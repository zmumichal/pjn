import codecs
from pjn import get_distance


class ErrorStats:
    def __init__(self, errors_filename):
        self.total_count = 0
        self.len_to_count = {}

        for line in codecs.open(errors_filename, encoding='utf8', errors='ignore').readlines():
            self.total_count += 1
            entry = line.split(';')
            len = get_distance(entry[0], entry[1])
            if self.len_to_count.has_key(len):
                self.len_to_count[len] += 1
            else:
                self.len_to_count[len] = 1

    def getProbabilityOfDistance(self, len):
        tmp = filter(lambda x: x[0] <= len, self.len_to_count.iteritems())
        var = reduce(lambda sum, entry: sum + entry[1], tmp, 0)
        return float(self.total_count - var)/self.total_count



# error_stats = ErrorStats('lab3/bledy.txt')
# print error_stats.getProbabilityOfDistance(1)