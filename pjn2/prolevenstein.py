# -*- coding: utf-8 -*-
from sys import maxint

DIACRITIC_COST = 0.1
ORTHOGRAPHIC_COST = 0.2
CZECH_COST = 0.5
TYPO_COST = 0.7

diacritic = [
    {u"a", u"ą", },
    {u"c", u"ć", },
    {u"e", u"ę", },
    {u"ń", u"n", },
    {u"o", u"ó", },
    {u"s", u"ś", },
    {u"l", u"ł", },
    {u"z", u"ż", u"ź", },
]

orthographic = [
    {u"rz", u"ż", u"sz"},
    {u"sz", u"cz", },
    {u"u", u"ó", },
    {u"ś", u"ć", },
    {u"ś", u"ż", },
    {u"h", u"ch", },
    {u"ą", u"on", },
    {u"ę", u"en", },
    {u"ł", u"u", },
    {u"z", u"s", }
]


def letter_to_key_index(keyboard_map):
    letter_to_index = {}
    for i in range(len(keyboard_map)):
        for j in range(len(keyboard_map[i])):
            letter_to_index[keyboard_map[i][j]] = [i, j]
    return letter_to_index


normal_key_to_index = letter_to_key_index([
    [u"q", u"w", u"e", u"r", u"t", u"y", u"u", u"i", u"o", u"p", ],
    [u"a", u"s", u"d", u"f", u"g", u"h", u"j", u"k", u"l", ],
    [u"z", u"x", u"c", u"v", u"b", u"n", u"m", ]
])
special_key_to_index = letter_to_key_index([
    [u"π", u"œ", u"ę", u"©", u"ß", u"←", u"↓", u"→", u"ó", u"þ", ],
    [u"ą", u"ś", u"ð", u"æ", u"ŋ", u"’", u"ə", u"…", u"ł", ],
    [u"ż", u"ź", u"ć", u"„", u"”", u"ń", u"µ", ],
])


def standard_cost(array, lhs, lhs_count, rhs, rhs_count):
    diff = 0 if lhs[lhs_count - 1] == rhs[rhs_count - 1] else 1
    tmp = min(array[lhs_count][rhs_count - 1] + 1,
              array[lhs_count - 1][rhs_count] + 1,
              array[lhs_count - 1][rhs_count - 1] + diff)
    current = array[lhs_count][rhs_count]
    array[lhs_count][rhs_count] = tmp if tmp < current else current


def apply_swaps(swaps, swap_cost, array, lhs_count, lhs_one, lhs_two, rhs_count, rhs_one, rhs_two):
    for possible in swaps:
        has_lhs_one = lhs_one in possible
        has_rhs_one = rhs_one in possible
        has_lhs_two = lhs_two in possible
        has_rhs_two = rhs_two in possible

        if has_lhs_one and has_rhs_one:
            tmp = array[lhs_count - 1][rhs_count - 1] + swap_cost
            current = array[lhs_count][rhs_count]
            array[lhs_count][rhs_count] = tmp if tmp < current else current

        if has_lhs_one and has_rhs_two:
            tmp = array[lhs_count - 1][rhs_count - 2] + swap_cost
            current = array[lhs_count][rhs_count]
            array[lhs_count][rhs_count] = tmp if tmp < current else current

        if has_lhs_two and has_rhs_one:
            tmp = array[lhs_count - 2][rhs_count - 1] + swap_cost
            current = array[lhs_count][rhs_count]
            array[lhs_count][rhs_count] = tmp if tmp < current else current

        if has_lhs_two and has_rhs_two:
            tmp = array[lhs_count - 2][rhs_count - 2] + swap_cost
            current = array[lhs_count][rhs_count]
            array[lhs_count][rhs_count] = tmp if tmp < current else current


def apply_czech(cost, array, lhs_count, lhs_two, rhs_count, rhs_two):
    if len(lhs_two) == 2 and len(rhs_two) == 2:
        if lhs_two[0] == rhs_two[1] and lhs_two[1] == rhs_two[0]:
            tmp = array[lhs_count - 2][rhs_count - 2] + cost
            current = array[lhs_count][rhs_count]
            array[lhs_count][rhs_count] = tmp if tmp < current else current


def apply_typo(key_to_index, cost, array, lhs_count, lhs_one, rhs_count, rhs_one):
    lhs_index = key_to_index[lhs_one] if key_to_index.has_key(lhs_one) else [100000, 100000]
    rhs_index = key_to_index[rhs_one] if key_to_index.has_key(rhs_one) else [-200, -200]
    if max(abs(lhs_index[0] - rhs_index[0]), abs(lhs_index[1] - rhs_index[1])) == 1:
        tmp = array[lhs_count - 1][rhs_count - 1] + cost
        current = array[lhs_count][rhs_count]
        array[lhs_count][rhs_count] = tmp if tmp < current else current


def levenstein(lhs, rhs, fail_after=100, precomputed=[]):
    lhs_length = len(lhs)
    rhs_length = len(rhs)
    array = [[1000 for x in range(0, rhs_length + 1)] for x in range(0, lhs_length + 1)]

    precomputed_len = len(precomputed)
    for lhs_count in range(0, precomputed_len):
        array[lhs_count] = precomputed[lhs_count]
    for lhs_count in range(precomputed_len, lhs_length + 1):
        array[lhs_count][0] = lhs_count
    if precomputed_len == 0:
        for rhs_count in range(0, rhs_length + 1):
            array[0][rhs_count] = rhs_count

    for lhs_count in range(precomputed_len, lhs_length + 1):
        min_at_front = lhs_count
        for rhs_count in range(1, rhs_length + 1):
            lhs_two = lhs[lhs_count - 2:lhs_count].lower()
            rhs_two = rhs[rhs_count - 2:rhs_count].lower()
            lhs_one = lhs[lhs_count - 1].lower()
            rhs_one = rhs[rhs_count - 1].lower()

            if True:
                if lhs_one == rhs_one:
                    tmp = array[lhs_count - 1][rhs_count - 1]
                    current = array[lhs_count][rhs_count]
                    array[lhs_count][rhs_count] = tmp if tmp < current else current

            if True:
                apply_swaps(diacritic, DIACRITIC_COST, array, lhs_count, lhs_one, lhs_two, rhs_count, rhs_one, rhs_two)

            if True:
                apply_swaps(orthographic, ORTHOGRAPHIC_COST, array, lhs_count, lhs_one, lhs_two, rhs_count, rhs_one,
                            rhs_two)

            if True:
                apply_czech(CZECH_COST, array, lhs_count, lhs_two, rhs_count, rhs_two)

            if True:
                apply_typo(normal_key_to_index, TYPO_COST, array, lhs_count, lhs_one, rhs_count, rhs_one)
                apply_typo(special_key_to_index, TYPO_COST, array, lhs_count, lhs_one, rhs_count, rhs_one)

            if True:
                standard_cost(array, lhs, lhs_count, rhs, rhs_count)

            min_at_front = array[lhs_count][rhs_count] if array[lhs_count][rhs_count] < min_at_front else min_at_front
        if min_at_front >= fail_after:
            for new_lhs_count in range(lhs_count, lhs_length + 1):
                for new_rhs_count in range(1, rhs_length + 1):
                    array[new_lhs_count][new_rhs_count] = maxint
            return array

    return array
