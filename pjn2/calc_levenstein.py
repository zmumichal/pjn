import locale
import sys
from prolevenstein import levenstein

while True:
    first = raw_input("first: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
    second = raw_input("second: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
    tab = levenstein(first, second)
    print tab[len(first)][len(second)]
    print tab