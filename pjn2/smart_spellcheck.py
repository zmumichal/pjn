# -*- coding: utf-8 -*-
import locale
import sys
import time
import itertools
from prolevenstein import levenstein
import cProfile
from operator import itemgetter


prefixes = open("lab2/pocz.utf8.dat").readlines()
grouped_suffixes = map(lambda line: line.decode('utf8').split(';')[2].split(':'),
                       open("lab2/konc.utf8.dat").readlines())
RETAINED_COUNT = 600


def main(to_correct):
    start = time.clock()

    qualified = [[len(to_correct), []] for x in range(0, RETAINED_COUNT)]
    prev_line = u''
    prev_array = levenstein(to_correct, u'')
    worst_score = len(to_correct)
    worst_index = 0

    for line in prefixes:
        tuple = line.decode('utf8').strip().split(':')
        token = tuple[0]

        common_prefix_length = ([x[0] == x[1] for x in zip(token, prev_line)] + [0, ]).index(0)

        already_computed = prev_array[:common_prefix_length]
        array = levenstein(token, to_correct, worst_score, already_computed)

        score = array[len(token)][len(to_correct)]
        if score < worst_score:
            # print qualified[worst_index], worst_score, token, score, time.clock() - start

            suffixes = grouped_suffixes[int(tuple[1])] if len(tuple) > 1 else [""]
            qualified[worst_index] = [score, map(lambda suffix: token + suffix, suffixes)]
            worst_score = score
            for i in range(0, len(qualified)):
                if qualified[i][0] > worst_score:
                    worst_score = qualified[i][0]
                    worst_index = i

        prev_array = array
        prev_line = token

    qualified = map(lambda (score, tokens): tokens, qualified)
    qualified = list(itertools.chain(*qualified))
    result = map(lambda candidate: [candidate, levenstein(to_correct, candidate)[len(to_correct)][len(candidate)]],
                 qualified)
    result.sort(key=itemgetter(1))
    # print result, time.clock() - start
    for entry in result:
        print entry[1], '\t', entry[0]
    print "\nBEST MATCH: ", result[0][0], " //with score: ", result[0][1], "\n\n"


to_correct = raw_input("Feed line: ").decode(sys.stdin.encoding or locale.getpreferredencoding(True))
cProfile.run('main(to_correct)')